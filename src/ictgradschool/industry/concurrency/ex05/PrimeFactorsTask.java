package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable{
    public enum TaskState{
        INITIALIZED, COMPLETED, ABORTED;
    }

    long n;
    List<Long> factors;
    TaskState taskState;

    public PrimeFactorsTask(long n){
        this.n = n;
        this.factors = new ArrayList<Long>();
        this.taskState = TaskState.INITIALIZED;
    }

    @Override
    public void run(){
        //Code implemented with reference at https://introcs.cs.princeton.edu/java/13flow/Factors.java.html
        // for each potential factor
        for (long factor = 2; factor*factor <= n; factor++) {
            if(Thread.interrupted()){
                this.taskState=TaskState.ABORTED;
                return;
            }
            // if factor is a factor of n, repeatedly divide it out
            while (n % factor == 0) {
                factors.add(factor);
                n = n / factor;
            }
        }
        // if biggest factor occurs only once, n > 1
        if (n > 1) factors.add(n);

        this.taskState=TaskState.COMPLETED;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException{
        return this.taskState==TaskState.COMPLETED?this.factors:(List<Long>) new IllegalStateException();
    }

    public long n(){
        return this.n;
    }

    public TaskState getTaskState(){
        return this.taskState;
    }
}