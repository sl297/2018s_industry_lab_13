package ictgradschool.industry.concurrency.ex05;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import ictgradschool.Keyboard;
import ictgradschool.industry.concurrency.examples.example03.PrimeFactors;

public class PrimeFactorsApp{
    public static void main(String[] args) throws InterruptedException{
        //提示用户键入一个长整数
        System.out.println("Input your long number: ");
        //获取长整数的值并由一个局部变量保存
        Long tempN = Long.parseLong( Keyboard.readInput());
        //通过用户键入的数据，实例化runnable的任务
        PrimeFactorsTask primeFactorsTask = new PrimeFactorsTask(tempN);
        //实例化线程对象，runThread用于计算factors
        Thread runThread = new Thread(primeFactorsTask);
        //实例化线程对象，abortThread用于监听用户输入，这里直接在参数中新建了一个Runnable对象
        Thread abortThread = new Thread(new Runnable(){
            //直接新建的Runnable对象作参数，覆写run()方法
            @Override
            public void run() {
                //提示用户可以通过单击ENTER中断计算
                System.out.println("Abort computation by press ENTER key...");
                //检测用户输入
                while(true){
                    //该步再检测一下计算任务的状态，如果完成就join()使线程回归夫线程
                    //当然不是完全需要这一步，只是运行到此处时有时间和资源让计算任务计算，万一到这一步时就算好了就可以直接返回了。
                    if(primeFactorsTask.getTaskState()== PrimeFactorsTask.TaskState.COMPLETED) {
                        //中断当前线程 - abortThread
                        Thread.currentThread().interrupt();
                        return;
                    }

                    //如果任务仍在计算，获取用户输入，查看用户是否想中断线程
                    try {
                        //从标准输入中获取用户的输入是否按了ENTER键
                        //用Keyboard.readInput()也可以，检查是不是空字符“”
                        int i = System.in.read();
                        //如果用户按压了ENTER键
                        if(i==10){
                            //计算任务的线程中断
                            runThread.interrupt();
                            //当前线程也中断
                            Thread.currentThread().interrupt();
                            //返回
                            return;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //The usual main thread that starts the other two threads and which waits for the computation thread to complete.
        //Step 2 - 开启runThread计算prime factors
        runThread.start();
        //Step 3 - 增强，中断线程的线程开始
        abortThread.start();

        //Step 2 - runThread停止
        /*
         * To wait for the ​ Thread ​ object (used to run the ​ PrimeFactorsTask​ ) to terminate –
         * either because the associated ​ PrimeFactorsTask ​ object’s ​ run()​ method
         * completed normally or was aborted, use ​ Thread​ ’s ​ join()​ method.
         * 1. 如果进程被中断，通过join()将runThread进程加入夫进程(主进程)，此时返回主进程。
         * 2. 如果完成计算，正常结束，runThread的run()会将进程带到Dead状态，此时返回主进程。
         */
        while(true) {
            //如果线程被中断
            if(runThread.isInterrupted()) {
                //runThread回归夫线程
                runThread.join();
                break;
            }else if(primeFactorsTask.getTaskState()== PrimeFactorsTask.TaskState.COMPLETED){ //如果计算任务完成
                //abortThread(如果不ENTER中断那目前状态应该是Running，如果中断那目前状态应该是Blocked)手动join()回归夫线程
                //runThread(目前状态应该是Dead，因为是Dead，所以应该不用手动join()回归，自己自动回归)回归夫线程
                abortThread.join();
                //runThread.join();
                break;
            }
        }

        //完成两个线程后，开始输出计算结果
        if(primeFactorsTask.getTaskState()==PrimeFactorsTask.TaskState.COMPLETED) {
            //获取factors的列表
            List<Long> factors = primeFactorsTask.getPrimeFactors();
            System.out.println("Prime Factors are: ");
            //打印factors的列表
            for(Long l: factors){
                System.out.print(l+"   ");
            }
        }else{
            //如果计算尚未完成被用户中断则提示用户
            System.out.println("User interrupted computing...");
        }
    }

}