package ictgradschool.industry.concurrency.ex03;

import java.util.concurrent.ThreadLocalRandom;

public class PIRunnable implements Runnable {
    long counter = 0;
    long numSamples;

    public PIRunnable(long numSamples){
        this.numSamples = numSamples;

    }


    @Override
    public void run() {
        // We need use a for-loop to generate x and y and check if the distance is less than 2
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        for (long i = 0; i < numSamples; i++) {

            double x = tlr.nextDouble();
            double y = tlr.nextDouble();

            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                counter++;
            }

        }

    }


    public long getCounter() {
        return counter;
    }
}
