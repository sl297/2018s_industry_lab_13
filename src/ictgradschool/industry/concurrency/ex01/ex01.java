package ictgradschool.industry.concurrency.ex01;

public class ex01 {
    public static void main(String[] args) throws InterruptedException {
        Runnable my = new Runnable() {
            @Override
            public void run() {
                for(int a =0;a<=1_000_000;a++){
                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }

                    System.out.println(a);
                }
            }
        };


        Thread t = new Thread(my);
        t.start();

        Thread.sleep(500);
        t.interrupt();


    }

}





