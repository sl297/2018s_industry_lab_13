package ictgradschool.industry.concurrency.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {
    public static void main(String[] args) throws InterruptedException {
        // Acquire Transactions to process.
        List<Transaction> transactions = TransactionGenerator.readDataFile();

        // Create BankAccount object to operate on.
        BankAccount account = new BankAccount();

        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);

        // create 2 consumers
        ArrayList<Thread> consumers = new ArrayList<>();
        for(int i = 0; i < 2; i++){
            Consumer consumer = new Consumer(account,queue);
            Thread t = new Thread(consumer,"Consumer #" + i);// Threads can be assigned an arbitrary name so we can keep track of them.
            t.start();
            consumers.add(t);
        }

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Stuff & things
                for(Transaction t : transactions){
                    try {
                        queue.put(t);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        /* Start the producer and wait for it to finish */
        producer.start();
        producer.join();

        /* Tell the consumers they should stop soon */
        for (Thread consumer : consumers) {
            consumer.interrupt();
        }

        /* Wait for the consumers to stop */
        for (Thread consumer : consumers) {
            consumer.join();
        }

        System.out.println(account.getFormattedBalance());
    }
}