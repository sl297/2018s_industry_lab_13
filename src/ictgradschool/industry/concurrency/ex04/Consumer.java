package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    BankAccount account;
    BlockingQueue<Transaction> queue;

    public Consumer(BankAccount account, BlockingQueue<Transaction> queue) {
        this.account = account;
        this.queue = queue;
    }

    @Override
    public void run() {
        boolean shouldRun = true;

        /* Keep looping until we are both interrupted and the queue is empty - this is so that we don't leave
         * any transactions unprocessed (queue drain)  */
        while (shouldRun || !queue.isEmpty()) {
            try {
                Transaction transaction = queue.take();
                SerialBankingApp.doTransaction(transaction, account);
            } catch (InterruptedException e) {
                /* An InterruptedException clears the interrupted state, so we should set a flag here to
                 * indicate that we should stop execution soon */
                shouldRun = false;
            }
        }
    }
}